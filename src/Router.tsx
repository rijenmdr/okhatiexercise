import React, { useEffect, useState } from 'react'
import { Switch,Route,Redirect } from 'react-router-dom'
import Form from './pages/Form'
import Home from './pages/Home'

const Router=()=> {
    return (
        <Switch>
            <Route exact path="/" render={() => (
            <Redirect to="/register"/>
             )}/>
            <Route path="/home" component={Home}/>
            <Route path="/login"component={Form}/>
            <Route path="/register" component={Form}/>
        </Switch>
    )
}

export default Router
