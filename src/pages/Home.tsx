import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import {
    Card ,
    CardContent,
    Typography,
    CardActions,
    Button,
    makeStyles
    } from '@material-ui/core';

    const useStyles = makeStyles({
        root: {
          minWidth: 275,
          display:"flex",
          justifyContent:"space-between"
        },
        title: {
          fontSize: 20,
        },
      });

function Home() {
    const history = useHistory()
    const classes = useStyles()
    useEffect(() => {
        const authentication = Boolean(window.localStorage.getItem('authorized')) ?? false
        if(!authentication){
            history.push('/login')
        }
    }, [])
    
    const logOutHandler = () => {
        window.localStorage.removeItem('authorized');
        history.push('/login')
    }
    return (
        <Card className={classes.root} variant="outlined">
        <CardContent>
          <Typography className={classes.title} color="textSecondary" gutterBottom>
            Welcome User
          </Typography>
        </CardContent>
        <CardActions>
          <Button size="small" onClick={logOutHandler}>Logout</Button>
        </CardActions>
      </Card>
    )
}

export default Home
