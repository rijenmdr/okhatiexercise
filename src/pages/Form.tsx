import React, { FormEvent, useState } from 'react'
import { TextField,
  Button,
  Container,
  Grid,
  makeStyles,
  Typography,
  Avatar,
  Snackbar,
  IconButton,
  Link
  } from '@material-ui/core';
import { useHistory } from 'react-router';

export type FormState = {
    email : string
    password : string
    confirmPassword : string
}

const useStyles = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: "flex",
      flexDirection: "column",
      alignItems: "center"
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main
      },
    form: {
      width: "100%", 
      marginTop: theme.spacing(3)
    },
    submit: {
      margin: theme.spacing(3, 0, 2)
    },
    close: {
      padding: theme.spacing(0.5),
    }
  }));

const Form=()=> {
    let history = useHistory();
    let path =window.location.pathname.split('/')[1]

    const [value,setValue] = useState<FormState>({
        email:"",
        password:"",
        confirmPassword:""
    })
    const [error,setError] = useState({
        email:"",
        password:"",
        confirmPassword:""
    })
    const [open,setOpen] = useState(false)
    const [message,setMessage] = useState("")
    const classes = useStyles();

    const onChangeHandler = (e:React.
        ChangeEvent<HTMLInputElement>) => {
        setValue({
            ...value,
            [e.target.name] : e.target.value,
        })
    }
    
    
    function validate(){
        let input = value;
        let errors = {
            "email":"",
            "password":"",
            "confirmPassword":""
        };
        let isValid = true;
        
        if (!input["email"]) {
          isValid = false;
          errors["email"] = "Please enter your email Address.";
        }
    
        if (typeof input["email"] !== "undefined") {
            
          var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
          if (!pattern.test(input["email"])) {
            isValid = false;
            errors["email"] = "Please enter valid email address.";
          }
        }
    
        if (!input["password"]) {
          isValid = false;
          errors["password"] = "Please enter your password.";
        }

        if (typeof input["password"] !== "undefined") {
            
            var pattern = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/
            if (!pattern.test(input["password"])) {
              isValid = false;
              errors["password"] = "Please enter valid password address.";
            }
          }
    
        if (!input["confirmPassword"] && path==="register") {
          isValid = false;
          errors["confirmPassword"] = "Please enter your confirm password.";
        }
    
        if (typeof input["password"] !== "undefined" && typeof input["confirmPassword"] !== "undefined" && path==="register") {
            
          if (input["password"] != input["confirmPassword"]) {
            isValid = false;
            errors["confirmPassword"] = "Passwords don't match.";
          }
        } 
    
       setError(errors)
    
        return isValid;
    }
    
    const handleSubmit = ()=>{
        if(validate()){
          if(path==="register"){
            const user = {
              email:value.email,
              password:value.password
            }
            window.localStorage.setItem('user', JSON.stringify(user));
            setMessage("Registration Successful")
            setOpen(true)
            history.push('/login')
          } else{
            let authenticatedUser ={
              email:"",
              password:""
            }
            authenticatedUser = JSON.parse(window.localStorage.getItem('user')??"");
            if(authenticatedUser.email === value.email && authenticatedUser.password ===value.password){
              window.localStorage.setItem('authorized', 'true');
              history.push('/home')
              setMessage("Login Successful")
              setOpen(true)
            } else{
              setMessage("Login Failure")
              setOpen(true)
            }
          }
          setValue({
            email:'',
            password:'',
            confirmPassword:''
          })
        }
    }

   
  const handleClose = (event: React.SyntheticEvent | MouseEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
    setMessage("")
  };

  
    return (
        <div>
            <Container component="main" maxWidth="xs">
            <div className={classes.paper}>
              <Snackbar 
                open={open}
                autoHideDuration={6000}
                onClose={handleClose}
                message={message}
                action={
                  <React.Fragment>
                    <IconButton
                      aria-label="close"
                      color="inherit"
                      className={classes.close}
                      onClick={handleClose}
                    >
                      x
                    </IconButton>
                  </React.Fragment>
                }/>
                <Avatar className={classes.avatar}></Avatar>
                <Typography component="h1" variant="h5">
                    {path==="login"?'Sign in':'Sign up'}
                </Typography>
                <form className={classes.form}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField name="email"
                            type="email" 
                            label="Email" 
                            required 
                            value={value.email} 
                            onChange={onChangeHandler} 
                            error={error.email?true:false}
                            fullWidth
                            variant="outlined"
                            helperText={error.email}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField name="password" type="password" required 
                            label="Password"
                            value={value.password} 
                            onChange={onChangeHandler}
                            error={error.password?true:false}
                            fullWidth
                            variant="outlined"
                            helperText={error.password}
                            />
                        </Grid>
                        {path==="register" &&
                        <Grid item xs={12}>
                            <TextField name="confirmPassword" 
                            type="password" label="Confirm Password" required 
                            value={value.confirmPassword} onChange={onChangeHandler}
                            error={error.confirmPassword?true:false}
                            fullWidth
                            variant="outlined"
                            helperText={error.confirmPassword}
                        />
                        </Grid>
                        }
                        <Grid item xs={12}>
                            <Button variant="contained"
                            color="primary"
                            className={classes.submit}
                            fullWidth
                            onClick={handleSubmit}
                            >
                                {path==="login"?'Sign In':'Sign Up'}
                            </Button>
                        </Grid>
                        {path==="login" ?
                        <Link href="/register">
                          Don't have an account ?
                        </Link> :
                        <Link href="/login">
                          Already have an account ?
                       </Link>
                        }
                    </Grid>
                </form>
            </div>
            </Container>
        </div>
    )
}

export default Form
